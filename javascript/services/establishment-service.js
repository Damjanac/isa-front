angular.module("isaProject")

.factory("EstablishmentService", ["$http", "$rootScope", function($http, $rootScope){

    return{

        getAllCinemas: function() {
			return $http({
				method: 'GET',
				url: '/cinemas'
			})
        },

        getAllTheatres: function() {
            return $http({
                method: 'GET',
                url: '/theatres'
            })
        },

        getEstablishmentById: function(id) {
            return $http({
                method: 'GET',
                url: '/establishments/' + id
            })
        },

        getEstablishmentRating: function(id) {
            return $http({
                method: 'GET',
                url: '/establishments/' + id + '/rating'
            })
        },

        getEstablishmentIncome: function(id,data) {
            return $http({
                method: 'POST',
                url: '/establishments/' + id + '/income',
                data: data
            })
        },

        updateEstablishment: function(id, data) {
            return $http({
                method: 'PUT',
                url: '/establishments/' + id,
                data: data
            })
        },

        getAllCinemasEvents: function(id) {
            return $http({
                method: 'GET',
                url: '/cinemas/' + id + '/events'
            })
        },

        getCinemaRepertoire: function(establishmentId, dayOffset) {
            return $http({
                method: 'GET',
                url: '/establishments/' + establishmentId +"/"+ dayOffset + '/events'
            })
        },

        getTheatreRepertoire: function(establishmentId) {
            return $http({
                method: 'GET',
                url: '/establishments/' + establishmentId + '/events'
            })
        },

        getEvent: function(establishmentId, eventId) {
            return $http({
                method: 'GET',
                url: '/establishments/' + establishmentId + '/events/' + eventId
            })
        },

        createEvent: function(establishmentId, data) {
            return $http({
                method: 'POST',
                url: '/establishments/' + establishmentId + '/events',
                data: data
            })
        },

        updateEvent: function(establishmentId, eventId, data) {
            return $http({
                method: 'PUT',
                url: '/establishments/' + establishmentId + '/events/' + eventId,
                data: data
            })
        },

        deleteEvent: function(establishmentId, eventId) {
            return $http({
                method: 'DELETE',
                url: '/establishments/' + establishmentId + '/events/' + eventId
            })
        },

        getHalls: function(establishmentId) {
            return $http({
                method: 'GET',
                url: '/establishments/' + establishmentId +'/halls'
            })
        },

        addEventDetails: function(eventId, data) {
            return $http({
                method: 'POST',
                url: '/events/' + eventId + '/details',
                data: data
            })
        },

        updateEventDetails: function(eventId, detailsId, data) {
            return $http({
                method: 'PUT',
                url: '/events/' + eventId + '/details/' + detailsId,
                data: data
            })
        },

        deleteEventDetails: function(eventId, detailsId) {
            return $http({
                method: 'DELETE',
                url: '/events/' + eventId + '/details/' + detailsId
            })
        },

        getDiscountedTickets: function(establishmentId) {
            return $http({
                method: 'GET',
                url: '/establishments/' + establishmentId + '/discountedtickets'
            })
        },

        createDiscountedTicket: function(data) {
            return $http({
                method: 'POST',
                url: '/discountedtickets',
                data: data
            })
        },

        deleteDiscountedTicket: function(id) {
            return $http({
                method: 'DELETE',
                url: '/discountedtickets/' + id,
            })
        },

        reserveDiscountedTicket: function(ticketId, data) {
            return $http({
                method: 'PUT',
                url: '/discountedtickets/' + ticketId,
                data: data
            })
        },

        getSeatReservations: function(establishmentId, eventId, scheduleId) {
        	return $http({
        		method : 'GET',
        		url : '/seatReservation/' + establishmentId + '/' + eventId + '/'+ scheduleId
        	})

        },

        confirmReservation: function(scheduleId, reservation) {
        	return $http({
        		method : 'POST',
        		url : '/confirmReservation/'+ scheduleId , 
        		data : reservation
        	})

        }

    }
}]);
